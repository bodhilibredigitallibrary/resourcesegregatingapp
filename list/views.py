from django.shortcuts import render, redirect
from .models import Uncuratedlist

def home(request):
    Uncuratedlists = Uncuratedlist.objects.all()
    return render(request, 'list/home.html', {'Uncuratedlists': Uncuratedlists})

def update(request, ListId):
    list = Uncuratedlist.objects.get(ListId=ListId)
    list.Status = True
    list.save()
    return redirect('home')