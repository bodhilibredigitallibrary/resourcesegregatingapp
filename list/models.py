from django.db import models
from django.conf import settings
from django.utils import timezone



# Create your models here.


class Language(models.Model):
    Lang_id = models.CharField(max_length=20)
    Lang_name = models.TextField(default="")
    def __str__(self):
        return self.Lang_name
    
class Credentials(models.Model):
    Credential_type = models.CharField(max_length=30)
    Credential_details = models.TextField(default="")
    def __str__(self):
        return self.Credential_type

class License(models.Model):
    License_id = models.CharField(max_length=20)
    License_name = models.CharField(max_length=100)
    License_commercial_allowed = models.BooleanField()
    def __str__(self):
        return self.License_id

class Agegroup(models.Model):
    Age_id = models.CharField(max_length=20)
    Age_name = models.TextField()
    def __str__(self):
        return self.Age_id

class Subject(models.Model):
    Subject_id = models.CharField(max_length=20)
    Subject_name = models.CharField(max_length=100)
    def __str__(self):
        return self.Subject_name

class Subsubject(models.Model):
    Subsubject_id = models.CharField(max_length=20)
    Subsubject_name = models.CharField(max_length=100)
    def __str__(self):
        return self.Subsubject_name

class Oer(models.Model):
    Oer_name = models.CharField(max_length=1000)
    Oer_description = models.TextField()
    Lang_name = models.ManyToManyField(Language)
    Oer_license = models.ForeignKey('License',on_delete=models.CASCADE,)
    Oer_public = models.BooleanField()
    Oer_download_url = models.URLField(max_length=200)
    Oer_credentials = models.ForeignKey('Credentials',on_delete=models.CASCADE,)
    Age_name = models.ManyToManyField(Agegroup)
    DOU =  models.DateField(null=True) #Date of Update or Date of Download
    Size = models.CharField(max_length=100,null=True)
    Subject_name = models.ManyToManyField(Subject)
    Susubbject_name = models.ManyToManyField(Subsubject)
    def __str__(self):
        return self.Oer_name
    #def get_oer(self):
        #return

class Uncuratedlist(models.Model):
    ListId = models.AutoField(primary_key=True,default=None)
    Name = models.CharField(max_length=1000)
    URL = models.URLField(max_length=200)
    Status = models.BooleanField(default=False)
    def __str__(self):
        return self.Name
