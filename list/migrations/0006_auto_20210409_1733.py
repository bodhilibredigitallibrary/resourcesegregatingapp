# Generated by Django 3.1.3 on 2021-04-09 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('list', '0005_uncuratedlist'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uncuratedlist',
            name='id',
        ),
        migrations.AddField(
            model_name='uncuratedlist',
            name='ListId',
            field=models.AutoField(default=1, primary_key=True, serialize=False),
            preserve_default=False,
        ),
    ]
