# Generated by Django 3.1.3 on 2020-11-23 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('list', '0002_delete_post'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='oer',
            name='Oer_language',
        ),
        migrations.AddField(
            model_name='credentials',
            name='Credential_details',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='language',
            name='Lang_name',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='oer',
            name='Lang_name',
            field=models.ManyToManyField(to='list.Language'),
        ),
    ]
