from django.contrib import admin
from list.models import *

# Register your models here.

@admin.register(Oer, Agegroup, License, Credentials, Language, Subject, Subsubject, Uncuratedlist)
class OerAdmin(admin.ModelAdmin):
    oer_display=['Oer_name','Oer_description','Age_id']